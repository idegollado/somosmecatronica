package com.somosmecatronica.app.server;


public class SomosMecatronicaPath {

	public static final String DOMAIN =  "https://smtest.herokuapp.com";
	public static final  String DOMAIN_PRODUCCTION = "http://www.somosmecatronica.com";
//verification payment program
	public class VerificationPayment{

		public static final String PATH = "/verification_payment.php";
		public static final String PARAM_ID = "payment_id";
	    public static final String PARAM_INFO = "payment_client_json";
	    public static final String PARAM_EMAIL = "email";
		public static final String RESPONSE_ERROR = "error";
	    public static final String RESPONSE_MESSAGE = "message";
	}

	public class DownloadRevista{

		public static final String PATH = "/download.php";
		public static final String PARAM_NOMBRE = "download_file";
		public static final String PARAM_VERSION = "type";
		public static final String PARAM_KEY = "key"; 
	}	
}