package com.somosmecatronica.app;

import android.app.Application;
import android.content.Context;
import android.os.Environment;

import com.parse.Parse;

/**
 * Created by dev on 3/17/15.
 */
public class GlobalApp extends Application {
    private static Context context;
    public void onCreate(){
        super.onCreate();
        GlobalApp.context = getApplicationContext();
        Parse.enableLocalDatastore(context);
        Parse.initialize(context,
                         context.getString(R.string.parse_app_id),
                         context.getString(R.string.parse_client_key)
                        );

    }

    public static Context getAppContext(){
        return GlobalApp.context;
    }

  /*Check if external storage is available for read and write*/
    public static boolean isExternalStorageWritable(){
        String state = Environment.getExternalStorageState();
        //si se quiere  ser mas especifico en los estado se puede usar un switch
        if(Environment.MEDIA_MOUNTED.equals(state)){
            return true;
        }
        return false;
    }

    public static boolean isExternalStorageReadable(){
        String state =  Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state) ||
            Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))  {
            return true;
        }
        return false;
    } 

}
