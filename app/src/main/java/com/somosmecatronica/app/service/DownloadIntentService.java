package com.somosmecatronica.app.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.app.IntentService;
import java.net.URL;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URLConnection;

import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.somosmecatronica.app.R;
import com.somosmecatronica.app.receiver.DownloadReceiver;
import com.somosmecatronica.app.receiver.DownloadReceiverDelegate;
import com.somosmecatronica.app.ui.activity.DownloaderActivity;
import com.somosmecatronica.app.ui.notification.NotificationBuilder;
import com.somosmecatronica.app.util.MyDownloadManager;
import com.somosmecatronica.app.util.consts.ExtraName;
import com.somosmecatronica.app.util.intent.DownloadIntentConstant;
import com.somosmecatronica.app.util.intent.BroadcastNotifier;
 
public class DownloadIntentService extends IntentService{

    private MyDownloadManager downloader;

	/**
     * An IntentService must always have a constructor that calls the super constructor. The
     * string supplied to the super constructor is used to give a name to the IntentService's
     * background thread.
     */
    public DownloadIntentService() {
		super("DownloadIntentService");
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
	protected void onHandleIntent(Intent workerIntent) {

		// Gets a URL to read from the incoming Intent's "data" value
        String localUrlString = workerIntent.getDataString();
        String edition = workerIntent.getExtras().getString(ExtraName.REVISTA_EDITION);

        if(localUrlString != null && edition != null){
        // A URL that's local to this method
            downloader = new MyDownloadManager(this, localUrlString, edition);
            downloader.execute();
        }
	}

}