package com.somosmecatronica.app.payment;

/**
 * Created by dev on 7/11/15.
 * @ Todo uso de esta clase sera cambiado por server.PathName
 */

public class PaymentServerData {

    public static final String URL = "https://smtest.herokuapp.com/verification_payment.php";
    public static  final String PRODUCTION_URL = "http://www.somosmecatronica.com/verification_payment.php";
    public static final String PARAM_ID = "payment_id";
    public static final String PARAM_INFO = "payment_client_json";
    public static final String PARAM_EMAIL = "email";

    public static final String RESPONSE_ERROR = "error";
    public static final String RESPONSE_MESSAGE = "message";

}
