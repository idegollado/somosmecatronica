package com.somosmecatronica.app.payment;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.somosmecatronica.app.server.SomosMecatronicaPath;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dev on 7/11/15.
 */
public class PaymentVolleyRequest {

    private RequestQueue requestQueue;
    private PaymentVerificationData data;
    private Response.Listener<String> response;
    private Response.ErrorListener responseError;

    public PaymentVolleyRequest(Context p1, Response.Listener<String> p2,
            Response.ErrorListener p3, PaymentVerificationData p4) {

        response = p2;
        responseError = p3;
        data = p4;
        requestQueue = Volley.newRequestQueue(p1);
    }

    public void verify(){
        RetryPolicy policy =
        new DefaultRetryPolicy(6000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        StringRequest request = createStringRequest();
        request.setRetryPolicy(policy);

        requestQueue.add(request);
    }

    /*private*/
    private StringRequest createStringRequest(){
        return new StringRequest(Request.Method.POST, SomosMecatronicaPath.DOMAIN + SomosMecatronicaPath.VerificationPayment.PATH,
                response,responseError){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                /*Parametros que necesita la direccion url para poder hacer
                * la validacion*/
                Map<String, String> params = new HashMap<String, String>();
                params.put(PaymentServerData.PARAM_ID, data.getId());
                params.put(PaymentServerData.PARAM_INFO, data.getPayment_info());
                params.put(PaymentServerData.PARAM_EMAIL,data.getEmail());

                return params;
            }
        };
    }
}
