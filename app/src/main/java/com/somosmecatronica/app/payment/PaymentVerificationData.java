package com.somosmecatronica.app.payment;

/**
 * Created by dev on 7/11/15.
 */
public class PaymentVerificationData {

    private String id;
    private String payment_info;
    private String email;


    public PaymentVerificationData(String pid, String ppayment_info, String pemail){
        id=pid;
        payment_info = ppayment_info;
        email=pemail;
    }

    public String getId() {
        return id;
    }

    public String getPayment_info() {
        return payment_info;
    }

    public String getEmail() {
        return email;
    }
}
