package com.somosmecatronica.app.main;

import java.util.Date;
/**
 * Created by dev on 3/2/15.
 */
public class SomosRevista extends SomosObject {

    public static final String DOMAIN_NAME = "SomosRevista";

    private String key;
    private String title;
    private int edition;
    private Date date;
    private int pags;
    private String iconName;
    private int state;
    private String fileName;
    private String fileUrl;
    private int price;
    private int id;



    public SomosRevista(){
        super();
    }

    public void setId(int id){id=id;}
    public int getId(){return id;}
    public void setKey(String key){ this.key = key;}
    public String getKey(){return key;};

    public String getTitle(){return title;}
    public void setTitle(String title) {
        this.title = title;
    }

    public int getEdition(){
           return edition;
    }
    public void setEdition(int edition) {
        this.edition = edition;
    }

    public Date getDate(){return date;}
    public void setDate(Date date) {this.date = date;}

    public int getPags() {return pags;}
    public void setPags(int pags) {this.pags = pags;}

    public String getFileUrl(){return fileUrl; }
    public void setFileUrl(String url){ fileUrl = url;}

    public String getFileName(){return fileName; }
    public void setFileName(String fileName1){ this.fileName = fileName1;  }

    public String getIconName() { return iconName;}
    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public int getState() {return state;}
    public void setState(int state) { this.state = state;}

    public int getPrice(){ return price;}
    public void setPrice(int precio){ price =precio;}



}
