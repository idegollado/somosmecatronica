package com.somosmecatronica.app.ui.activity;

import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;

import com.somosmecatronica.app.ui.fragment.NavigationDrawerFragment;
import com.somosmecatronica.app.R;
import com.somosmecatronica.app.ui.fragment.CatalogFragment;
import com.somosmecatronica.app.ui.fragment.DescargasFragment;
import com.somosmecatronica.app.ui.fragment.OnFragmentInteractionListener;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks,OnFragmentInteractionListener {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //syncroniza la app por si es la primera vez que se inicializa.
        //RevistaCloudData cloudData = new RevistaCloudData(this);
        //cloudData.setSynchronizator(new RevistaSync());//podria ser el activity quien implemente SYnc
        //cloudData.synchronize();


        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }
    //abre un fragment u otro.
    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction trans = fragmentManager.beginTransaction();
      //convertir a un log  Toast.makeText(this,"onNavigationDrawerItemSelected case " + position, Toast.LENGTH_SHORT).show();
        switch(position){
            case 0:
                trans.replace(R.id.container, CatalogFragment.newInstance(position + 1));
                break;
            case 1:
                trans.replace(R.id.container, DescargasFragment.newInstance(position + 1));
                break;
        }
        trans.commit();
    }

    public void onSectionAttached(int number) {
        String[] titles = getResources().getStringArray(R.array.menus);
      //convertir a un log  Toast.makeText(this,"case " + number, Toast.LENGTH_SHORT).show();
        switch (number) {
            case 1:
                mTitle = titles[number-1];
                break;
            case 2:
                mTitle = titles[number-1];
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setIcon(R.mipmap.sm);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            // getMenuInflater().inflate(R.menu.main, menu);
            // restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


}
