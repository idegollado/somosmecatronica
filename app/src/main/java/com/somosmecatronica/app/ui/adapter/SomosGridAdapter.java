package com.somosmecatronica.app.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.somosmecatronica.app.R;
import com.somosmecatronica.app.main.SomosRevista;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * Created by dev on 4/12/15.
 */
public class SomosGridAdapter extends BaseAdapter{
    private static final String TAG = "SomosGridAdapter";

    List<SomosRevista> revistas;

    Context context;
    ImageView imageView;
    LayoutInflater inflater;

    public SomosGridAdapter(Context thiz,List<SomosRevista> list){
        context = thiz;
        revistas = list;
    }

    @Override
    public int getCount() {
        //return the size of data.
        return revistas.size();
    }

    @Override
    public Object getItem(int position) {
        //return the position i of list
        return revistas.get(position);
    }

    @Override
    public long getItemId(int position) {
        //return the id
        return revistas.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //return the view of each element
        inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(convertView == null) {
            convertView = inflater.inflate(R.layout.list_item, parent, false);

            imageView = (ImageView) convertView.findViewById(R.id.imageview_rev);
            imageView.setImageBitmap(loadImage(position));
        }

        return convertView;
    }


    private Bitmap loadImage(int position) {
        Bitmap bitmap=null;
        SomosRevista revista = (SomosRevista) getItem(position);
        if(revista != null) {
            //Toast.makeText(context,"revista" + revista.getIconName(),Toast.LENGTH_SHORT).show();
            File file = context.getFileStreamPath(revista.getIconName());//probar que este archivo existe
            if (file.exists()) {
              try {
                    FileInputStream streamIn = context.openFileInput(revista.getIconName());
                    bitmap = BitmapFactory.decodeStream(streamIn);
                    streamIn.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
               Log.e(TAG, "Load default image");
            }
        }else{
            Log.e(TAG, "revista object null");
        }
        return bitmap;
    }
}
