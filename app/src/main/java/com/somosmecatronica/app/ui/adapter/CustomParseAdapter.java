package com.somosmecatronica.app.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQueryAdapter;
import com.somosmecatronica.app.R;

/**
 * Created by dev on 9/17/15.
 */
public class CustomParseAdapter extends ParseQueryAdapter<ParseObject> {

    public CustomParseAdapter(Context context, String className) {
        super(context, className);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getItemView(ParseObject object, View v, ViewGroup parent) {
        //to reuse
        if(v==null)
            v = View.inflate(getContext(), R.layout.catalog_item,null);
        ParseImageView imageView = (ParseImageView) v.findViewById(R.id.icon);
        ParseFile photoFile = (ParseFile) object.getParseFile("icon");
        if(photoFile != null){
            imageView.setParseFile(photoFile);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8,8,8,8);
            imageView.loadInBackground();
        }
        return super.getItemView(object, v, parent);
    }

}
