package com.somosmecatronica.app.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.content.LocalBroadcastManager;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.somosmecatronica.app.R;
import com.somosmecatronica.app.data.dao.SQLiteDescargaDAO;
import com.somosmecatronica.app.data.dao.SQLiteRevistaDAO;
import com.somosmecatronica.app.main.SomosDescarga;
import com.somosmecatronica.app.main.SomosRevista;
import com.somosmecatronica.app.receiver.DownloadReceiver;
import com.somosmecatronica.app.receiver.DownloadReceiverDelegate;
import com.somosmecatronica.app.server.SomosMecatronicaPath;
import com.somosmecatronica.app.service.DownloadIntentService;
import com.somosmecatronica.app.util.IOMethods;
import com.somosmecatronica.app.GlobalApp;

import com.somosmecatronica.app.util.consts.EstadoDescarga;
import com.somosmecatronica.app.util.consts.ExtraName;
import com.somosmecatronica.app.util.intent.DownloadIntentConstant;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;

/**
 * Estado:FEO, mucho por mejorar incluso por eliminar.
 *
 * Created by dev on 4/12/15.
 */
public class DownloaderActivity extends Activity implements DownloadReceiverDelegate{

    private static final String TAG = "DownloaderActivity";
    //EXTRAS
    Bundle extras;
    String fileName;
    String fileUrlPath;
    String imageUrlPath;
    String title;
    String key;
    String imageName;
    String pdfName;
    String edition;
    int price;

    long dateInMillis;

    //
    File pdfFile;
    PowerManager.WakeLock mWakeLock;
    ProgressBar progressBar;
    TextView tvWaitMessage;

    private DownloadReceiver downloadReceiver;

    Target target = new Target(){
        @Override
        public void onPrepareLoad(Drawable arg0){

        }

        @Override
        public void onBitmapFailed(Drawable arg0){

        }

        @Override
        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom arg1){

            new Thread(new Runnable() {
                @Override
                public void run() {

                    try{

                        FileOutputStream ostream = openFileOutput(imageName,Context.MODE_PRIVATE);
                        bitmap.compress(Bitmap.CompressFormat.JPEG,75,ostream);
                        ostream.close();
                    }catch(FileNotFoundException e){

                    }catch (Exception e){

                    }
                }
            }).start();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wait_layout);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar_wait);
        tvWaitMessage = (TextView) findViewById(R.id.tv_downloaing_message);
        progressBar.setVisibility(ProgressBar.INVISIBLE);

        extras = getIntent().getExtras();

        if(extras != null){
            key = extras.getString("extraKey");
            fileName = extras.getString("extraFileName");
            fileUrlPath = extras.getString("extraFileUrlPath");
            imageUrlPath = extras.getString("extraImageUrlPath");
            imageName = extras.getString("extraImageName");
            edition = extras.getString(ExtraName.REVISTA_EDITION);
            dateInMillis = extras.getLong("extraDate");
            title = extras.getString("extraTitle");
            price = extras.getInt("extraPrice");
            pdfName = edition + ".pdf";



            //descargar el completo.
            if(existPendingDescarga()){
                //init service and waiting.(receiver and so on).
                registerDownloadReceiver();
                //link puede ser de parte de la base de datos.
                initDownloadService(composeLinkWithParams(edition + ".pdf" ,"full","Pendiente"));
                progressBar.setVisibility(ProgressBar.VISIBLE);
                tvWaitMessage.setVisibility(View.VISIBLE);

            }else{
                //if fileName exists buscar el archivo en alacenamiento externo
                pdfFile = new File(getFilesDir(),pdfName);

                //leer el archivo completo o demo.
                if(pdfFile.exists() && pdfFile.isFile()){//size == sizeofRegularFile

                    showPdf(edition);
                }else{
                    //descargar el demo.

                    new DownloadTask(this).execute(composeLinkWithParams(edition + ".pdf","demo","Pendiente"));
                }
            }
        }else{
            ///no se envian los extras
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void started() {

    }

    @Override
    public void connecting() {

    }

    @Override
    public void parsing() {

    }

    @Override
    public void writing() {

    }

    @Override
    public void complete(String path, String fileName) {
        progressBar.setVisibility(ProgressBar.GONE);
        tvWaitMessage.setVisibility(View.GONE);
        showPdf(edition);
    }

    private void registerDownloadReceiver(){
        IntentFilter filter = new IntentFilter(DownloadIntentConstant.BROADCAST_ACTION);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        downloadReceiver = new DownloadReceiver(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(downloadReceiver,filter);
    }

    /*services*/
    private void initDownloadService(String link){
        //ok pues iniciar la descarga.


        Intent mServiceIntent  = new Intent(this, DownloadIntentService.class);
        mServiceIntent.setData(Uri.parse(link));
        mServiceIntent.putExtra(ExtraName.REVISTA_EDITION, edition);
        startService(mServiceIntent);
        //request.setTitle(edition + ".pdf");
    }

    /*compose link utils
   *@nombre el nombre del archivo que se desea descargar
   *@version se pide de ese archivo la version full o la demo.
   *@key la llave que se le da al usuario para acceder a este archivo.
   */
    private String composeLinkWithParams(String nombre1, String version2, String key3){
        /*el link a la descarga de el archivo tiene el siguiente formanto:
         * http://domain.com/name_and_path_of_file.php?param1=value1&param2=value2;
         */
        String linkDownload =  SomosMecatronicaPath.DOMAIN + SomosMecatronicaPath.DownloadRevista.PATH +
                "?" + SomosMecatronicaPath.DownloadRevista.PARAM_NOMBRE + "=" + nombre1 +
                "&" + SomosMecatronicaPath.DownloadRevista.PARAM_VERSION+ "=" + version2 +
                "&" + SomosMecatronicaPath.DownloadRevista.PARAM_KEY + "=" + key3;



        return linkDownload;
    }
    private class DownloadTask extends AsyncTask<String, Integer,Boolean> {

        String urlName;
        Context thiz;

        public  DownloadTask(Context context){
            thiz = context;
        }

        @Override
        protected  void onPreExecute(){
            super.onPreExecute();
            PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,getClass().getName());
            mWakeLock.acquire();
            progressBar.setVisibility(ProgressBar.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(String...str){
            urlName = str[0];//url to download a the file of internet
            //verfificar la conexio a internet
            ConnectivityManager connManager = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo netInfo = connManager.getActiveNetworkInfo();

            //verificar conexion antes de descargar
            boolean result = (netInfo != null) && netInfo.isConnected();//result 1

            if(result){

                try {
                    URL url = new URL(urlName);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    
                    if(GlobalApp.isExternalStorageWritable()){
                        FileOutputStream streamOut = openFileOutput(pdfName, Context.MODE_PRIVATE);
            
                        result = (connection.getResponseCode() == HttpURLConnection.HTTP_OK);

                        if(result) {


                            InputStream streamIn = connection.getInputStream();

                            IOMethods.writeStreamFromTo(streamIn, streamOut);
                           
                            connection.disconnect();

                        }
                     }
                }catch (MalformedURLException e){
                    //new URL(name)
                }catch(IOException e){//url.openConnection

                }

            }

            return result;
        }

        @Override
        protected void onPostExecute(Boolean result){
            //
            progressBar.setVisibility(ProgressBar.GONE);
            //descarga exitosa
            if(result){
                //aqui el registro.
                Picasso.with(thiz).load(imageUrlPath).into(target);
                registrarUnaRevistaEnLocal();
                showPdf(edition);

           }else{
                //show a dialog and try again or something else.
                Toast.makeText(thiz, "Error al Descargar, intente mas tarde", Toast.LENGTH_SHORT).show();
                finish();
           }

        }
    }
    private void registrarUnaRevistaEnLocal(){
        SomosRevista revista = new SomosRevista();
        revista.setDate(new Date(dateInMillis));
        revista.setFileUrl(fileUrlPath);
        revista.setEdition(Integer.parseInt(edition));
        revista.setKey(key);
        revista.setFileName(fileName);
        revista.setIconName(imageName);
        revista.setTitle(title);
        revista.setPrice(price);
        revista.setState(1);//DEMO

        SQLiteRevistaDAO revistaDAO = new SQLiteRevistaDAO();
        revistaDAO.insert(revista);
    }

    private void showPdf(String fileEdition){
        Intent intent = new Intent(DownloaderActivity.this, PdfActivity.class);
        intent.putExtras(extras);
        startActivity(intent);
    }

    private boolean existPendingDescarga(){
        SQLiteDescargaDAO sqliteDescargaQuery = new SQLiteDescargaDAO();

        List<SomosDescarga> descargasByRevistas =
                sqliteDescargaQuery.selectByRevista(Integer.parseInt(edition));
        if(descargasByRevistas != null) {
            int countDownloads = descargasByRevistas.size();
            //existe una, actualiza su estado.
            if (countDownloads == 1) {
                //no existe, registrala.
                SomosDescarga descarga = descargasByRevistas.get(0);

                if (descarga.getEstado() != EstadoDescarga.DOWNLOADED) {
                    //exist a pending download
                    return true;
                }
            }
        }
        return false;
    }
}
