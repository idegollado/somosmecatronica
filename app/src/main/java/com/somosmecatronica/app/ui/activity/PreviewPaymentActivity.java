package com.somosmecatronica.app.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.ProgressDialog;
import android.net.Uri;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import org.json.JSONException;
import org.json.JSONObject;
import java.math.BigDecimal;
import java.io.File;
import java.util.List;

import com.somosmecatronica.app.R;
import com.somosmecatronica.app.data.dao.SQLiteRevistaDAO;
import com.somosmecatronica.app.main.SomosRevista;
import com.somosmecatronica.app.payment.PaymentServerData;
import com.somosmecatronica.app.payment.PaymentVerificationData;
import com.somosmecatronica.app.payment.PaymentVolleyRequest;
import com.somosmecatronica.app.payment.PaypalGeneralContent;
import com.somosmecatronica.app.util.consts.ExtraName;
import com.somosmecatronica.app.util.mobile.settings.Cuenta;
import com.somosmecatronica.app.util.intent.DownloadIntentConstant;
import com.somosmecatronica.app.server.SomosMecatronicaPath;
import com.somosmecatronica.app.service.DownloadIntentService;
import com.somosmecatronica.app.receiver.DownloadReceiver;
import com.somosmecatronica.app.receiver.DownloadReceiverDelegate;
import com.somosmecatronica.app.ui.fragment.TryAgainDialog;
import com.somosmecatronica.app.util.storage.Image;

import android.app.DialogFragment;


public class PreviewPaymentActivity 
                        extends Activity 
                        implements Response.Listener<String>,
                                   Response.ErrorListener, 
                                   DownloadReceiverDelegate,
                                   TryAgainDialog.NoticeDialogListener{

    private static final String TAG = PreviewPaymentActivity.class.getSimpleName();

    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_PRODUCTION)
            .clientId(PaypalGeneralContent.PAYPAL_LIVE_CLIENT_ID)
            .acceptCreditCards(false)
            .merchantName("SomosMecatronica");

    //this manage notification and show pdf
    private DownloadReceiver downloadReceiver;
    //verification
    PaymentVerificationData verificationData;
    PaymentVolleyRequest paymentRequest;
    //values
    private int price;
    private String title;
    private String email="";
    private String edition;
    private String iconName;
    //views
    private TextView tvDesc;
    private TextView tvEmail;
    private Button btnGoPaypal;
    private CheckBox checkConds;
    private ImageView imageRevista;
    //dialogs
    private ProgressDialog waitVerification;
    private ProgressDialog waitDownload;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        tvDesc = (TextView) findViewById(R.id.tv_desc);
        tvEmail = (TextView) findViewById(R.id.tv_email);
        btnGoPaypal = (Button) findViewById(R.id.btn_go_paypal);
        checkConds = (CheckBox) findViewById(R.id.check_condiciones);
        imageRevista = (ImageView) findViewById(R.id.image_revista);
        //get info product and account.
        Bundle bundle = getIntent().getExtras();

        if(bundle != null){
            email = Cuenta.getGmail();//dentro solo por clarificar
            edition = bundle.getString(ExtraName.REVISTA_EDITION);
            //BUSCAR EN LA TABLA REVISTAS
            SQLiteRevistaDAO revistaQuery = new SQLiteRevistaDAO();
            List<SomosRevista> revistas = revistaQuery.selectByRevista(Integer.parseInt(edition));
            SomosRevista thisRevista= new SomosRevista();
            if(!revistas.isEmpty()){
                thisRevista = revistas.get(0);

            }
            price = thisRevista.getPrice();
            title = thisRevista.getTitle();
            iconName = thisRevista.getIconName();
            //OBTENER LA IMAGE DESDE EL ALMACENAMIENTO INTERNO
            File iconFile = getFileStreamPath(iconName);
            tvDesc.setText(getString(R.string.edition) + ": " + edition);
            imageRevista.setImageBitmap(Image.getBitmapFromFile(iconFile));
            tvEmail.setText(email);
            btnGoPaypal.setText("$"+price + " MXN");
        }
        //init paypal configurations.
        initPayPalService();
        registerDownloadReceiver();
    }

    @Override
    protected void onPause(){
        super.onPause();
    }
    @Override
    protected void onDestroy(){
        stopPayPalService();
         // If the DownloadStateReceiver still exists, unregister it and set it to null
        if (downloadReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(downloadReceiver);
            downloadReceiver = null;
        }
        
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK) {
            PaymentConfirmation confirmation = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if(confirmation != null){
                try{

                    Log.d(TAG, confirmation.toJSONObject().toString(4));
                    Log.d(TAG,confirmation.getPayment().toJSONObject().toString());
                    
                    String id = confirmation.toJSONObject().getJSONObject("response").getString("id");
                    String paymentInfo = confirmation.getPayment().toJSONObject().toString();
                     //verificar el pago en el server
                    verificationData = new PaymentVerificationData(id, paymentInfo, email);
                    paymentRequest = new PaymentVolleyRequest(this, this, this,verificationData);
                    paymentRequest.verify();

                    waitVerification = createProgressDialog("Pago","Verificando"); //tricky for progress dialog #Modify
                    waitVerification.show();
                }catch(JSONException e){
                    Log.e(TAG ,"Unfortunely an extremly unlikely failure ocurred" , e);
                }catch (Exception e){
                    Log.e(TAG, e.getMessage());
                }
            }
        }else if(resultCode == Activity.RESULT_CANCELED){

        }else if(resultCode == com.paypal.android.sdk.payments.PaymentActivity.RESULT_EXTRAS_INVALID) {

        }
    }

    /*Onclick*/
     public void onClickPaypalPayment(View view){
        PayPalPayment payPayment =
        new PayPalPayment(new BigDecimal(price), PaypalGeneralContent.Currency.MXN, title, PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(PreviewPaymentActivity.this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payPayment);
        startActivityForResult(intent, 0);
        Log.d(TAG, "onClick working");
    }

    public void onClickConditions(View view){
        //is the view now checked
        boolean checked = ((CheckBox) view).isChecked();

        switch (view.getId()) {
            case R.id.check_condiciones:
                    btnGoPaypal.setClickable(checked);
            break;
            default:
                break;
        }

    }

   
    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Error in server" + error.getMessage());
        Toast.makeText(this,"Error in Server",Toast.LENGTH_SHORT).show();
        hideProgressDialog(waitVerification);
        showTryAgainDialog();       
    }

    @Override
    public void onResponse(String response) {
        Log.d(TAG,response);
        
        try{
            JSONObject jsonResponse= new JSONObject(response);
            boolean error = jsonResponse.getBoolean(PaymentServerData.RESPONSE_ERROR);
            String message = jsonResponse.getString(PaymentServerData.RESPONSE_MESSAGE);
            Toast.makeText(this, message,Toast.LENGTH_SHORT).show();
            hideProgressDialog(waitVerification);
             
            if(error == false)
            {   //proceso de inicializar la descarga...
                quitarBotonComprar();
                waitDownload = createProgressDialog(getString(R.string.download_process), getString(R.string.wait));
                waitDownload.show();
                initDownloadService(composeLinkWithParams(edition + ".pdf" ,"full","Pendiente"));   
            }else{
               //que hace el usuario en esta situacion
            }
        }catch(JSONException e){
            Log.e(TAG,e.getMessage());
        }
    }

 /******************UTILS***************/

    /*services*/
    private void initDownloadService(String link){
        //ok pues iniciar la descarga.  
        Log.d(TAG, "initDownloadService()");
            
        Intent mServiceIntent  = new Intent(this, DownloadIntentService.class);
        mServiceIntent.setData(Uri.parse(link));
        mServiceIntent.putExtra(ExtraName.REVISTA_EDITION, edition);
        startService(mServiceIntent);
        //request.setTitle(edition + ".pdf");    
    }

    /*receivers*/
    private void registerDownloadReceiver(){
        IntentFilter filter = new IntentFilter(DownloadIntentConstant.BROADCAST_ACTION);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        downloadReceiver = new DownloadReceiver(this);
        // Registers the DownloadStateReceiver and its intent filters
        LocalBroadcastManager.getInstance(this).registerReceiver(downloadReceiver,filter);    
    }

    /*compose link utils
    *@nombre el nombre del archivo que se desea descargar 
    *@version se pide de ese archivo la version full o la demo.
    *@key la llave que se le da al usuario para acceder a este archivo.
    */
    private String composeLinkWithParams(String nombre1, String version2, String key3){
        /*el link a la descarga de el archivo tiene el siguiente formanto:
         * http://domain.com/name_and_path_of_file.php?param1=value1&param2=value2;
         */
        String linkDownload =  SomosMecatronicaPath.DOMAIN + SomosMecatronicaPath.DownloadRevista.PATH +
                          "?" + SomosMecatronicaPath.DownloadRevista.PARAM_NOMBRE + "=" + nombre1 +
                          "&" + SomosMecatronicaPath.DownloadRevista.PARAM_VERSION+ "=" + version2 +
                          "&" + SomosMecatronicaPath.DownloadRevista.PARAM_KEY + "=" + key3;
        
        return linkDownload;
    }

    private void openPdfActivity(String name){
        Intent intent = new Intent(PreviewPaymentActivity.this, PdfActivity.class);
        intent.putExtra(ExtraName.REVISTA_EDITION, name);
        try{
            Thread.sleep(2000);
            startActivity(intent);
        }catch(  InterruptedException e){
                        
        }
    }
/*Delegate*/
    @Override
    public void started(){
        changeMessage(waitDownload, getString(R.string.download_starting));
    }

    @Override
    public void connecting(){
        changeMessage(waitDownload, getString(R.string.download_connecting));
    }

    @Override
    public void parsing(){
        changeMessage(waitDownload, getString(R.string.download_starting));
    }

    @Override
    public void writing(){
        changeMessage(waitDownload, getString(R.string.download_downloaing));
    }

    @Override 
    public void complete(String path, String edition){
        Log.d(TAG, "complete: " + edition);
        hideProgressDialog(waitDownload);  
        openPdfActivity(edition);
    }
/*Dialogs */
///dialogs utils
    
    @Override
    public void onDialogPositiveClick(DialogFragment dialogFragment){
            waitVerification.show();
            paymentRequest.verify();
    }
    @Override    
    public void onDialogNegativeClick(DialogFragment dialogFragment){

    }

    public void showTryAgainDialog(){
        //create an instance of the dialog fragment and show it
        DialogFragment dialog = new TryAgainDialog();
        dialog.show(getFragmentManager(), "TryAgainDialog");
    }
    private ProgressDialog createProgressDialog(String title, String message){
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        return progressDialog;
    }

    private void hideProgressDialog(ProgressDialog progressDialog){
        if(progressDialog != null)
            progressDialog.dismiss();
    }

    private void changeMessage(ProgressDialog progressDialog , String message){
        if(progressDialog != null)
            progressDialog.setMessage(message);    
    }  

    private void quitarBotonComprar(){
        checkConds.setVisibility(View.GONE);
        btnGoPaypal.setVisibility(View.GONE);

    }

//paypal
    private void initPayPalService(){
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION,config);
        startService(intent);
    }

    private void stopPayPalService(){
        stopService(new Intent(this,PayPalService.class));
    }
  
}
