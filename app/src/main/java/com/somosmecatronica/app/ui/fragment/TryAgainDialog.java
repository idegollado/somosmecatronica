package com.somosmecatronica.app.ui.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.app.DialogFragment;

import com.somosmecatronica.app.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TryAgainDialog} interface
 * to handle interaction events.
 * Use the {@link TryAgainDialog#} factory method to
 * create an instance of this fragment.
 */
public class TryAgainDialog extends DialogFragment {
   
    private NoticeDialogListener mListener;

    public TryAgainDialog() {
        // Required empty public constructor
    }

    @Override
    public Dialog onCreateDialog(Bundle bundle){
        //use the builder class for convinenti dialog constuction
        AlertDialog builder = new AlertDialog.Builder(getActivity()).create();
        builder.setTitle("Verificacion");
        builder.setMessage("Ocurrio un error al verificar tu pago con los servidores de SomosMecatronica");
        builder.setCanceledOnTouchOutside(false);
        builder.setButton(AlertDialog.BUTTON_POSITIVE, "Intenta otra vez", 
            new DialogInterface.OnClickListener(){
                @Override
                 public void onClick(DialogInterface dialogInterface, int i){
                        mListener.onDialogPositiveClick(TryAgainDialog.this);
                 }   
            });
        return builder;
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);

        //cast activity para saber si implementa NoticeDialogListener
        try{
            mListener = (NoticeDialogListener) activity;
        }catch(ClassCastException  caste){
            throw new ClassCastException(activity.toString() + "must be implement NoticeDialogListener");
        }
    }

    public interface NoticeDialogListener{
        public void onDialogPositiveClick(DialogFragment dialogFragment);
        public void onDialogNegativeClick(DialogFragment dialogFragment);
    }
}
