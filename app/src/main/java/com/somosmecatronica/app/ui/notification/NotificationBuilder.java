package com.somosmecatronica.app.ui.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;
import android.widget.Toast;

import com.somosmecatronica.app.R;
import com.somosmecatronica.app.ui.activity.PdfActivity;
import com.somosmecatronica.app.util.consts.ExtraName;

import java.util.HashMap;

/**
 * Created by dev on 7/30/15.
 */
public class NotificationBuilder {

    private Context mThiz;
    private Intent mIntentResult;
    private NotificationCompat.Builder mBuilder;
    private PendingIntent mPendingIntent;
    private TaskStackBuilder mStackBuilder;
    private NotificationManager mNotificationManager;

    //message
    private String mText;
    private String mTitle;
    private final int ID=22342;
    private int mId= ID;
    private int mIntentFlag;
    private boolean mClickable =false;
    private boolean mAutoCancel=false;
    private Class<?> mClass;
    private HashMap<String,String> extrasMap;



    public NotificationBuilder(Context context) {
        mThiz = context;
        extrasMap = new HashMap<String,String>();
    }

    public void build(){
        mBuilder =
                new NotificationCompat.Builder(mThiz)
                .setSmallIcon(R.drawable.ic_stat_sm)
                .setContentTitle(mTitle)
                .setContentText(mText)
                .setAutoCancel(mAutoCancel);

        mIntentResult = new Intent(mThiz,mClass);
        mIntentResult.putExtra(ExtraName.REVISTA_EDITION, extrasMap.get(ExtraName.REVISTA_EDITION));

        mStackBuilder = TaskStackBuilder.create(mThiz);
        mStackBuilder.addParentStack(mClass);
        mStackBuilder.addNextIntent(mIntentResult);

        //If you want to start Activity when the user clicks the notification text in the
        //notiication drawer, you add the PendingIntnt by calling setContentIntent()
        mPendingIntent = mStackBuilder.getPendingIntent(0, mIntentFlag);
        mBuilder.setContentIntent(mPendingIntent);

        mNotificationManager = (NotificationManager) mThiz.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(mId,mBuilder.build());
    }
    public void setExtra(String extraName, String extraValue){
        extrasMap.put(extraName,extraValue);
    }
    public void setPendingIntentFlag(int flag){
        mIntentFlag = flag;
    }
    public void setTitle(String title){
        mTitle = title;
    }
    public void setMessage(String msg){
        mText = msg;
    }
    public void setAutoCancel(boolean cancelable){
        mAutoCancel =cancelable;
    }

    public void setClickable (boolean clickable){
        mClickable = clickable;
    }

    public void setActivityResult(Class<?> activity){
        mClass = activity;
    }

}
