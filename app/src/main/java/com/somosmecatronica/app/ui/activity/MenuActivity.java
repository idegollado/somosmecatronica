package com.somosmecatronica.app.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.somosmecatronica.app.R;

public class MenuActivity extends Activity {

    ImageView ivRevista;
    ImageView ivPrototipo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_v2);
        ivRevista = (ImageView) findViewById(R.id.imageRevista);
        ivPrototipo = (ImageView) findViewById(R.id.imagePrototipo);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void onClickRevista(View v){

        Intent intent = new Intent(MenuActivity.this, MainActivity.class);
        startActivity(intent);
    }

    //abre la lista de prototipos

    public void onClickPrototipo(View v){

        Intent intent = new Intent(MenuActivity.this, PrototipoListActivity.class);
        startActivity(intent);
    }
}
