package com.somosmecatronica.app.ui.activity;

import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.somosmecatronica.app.R;
import com.somosmecatronica.app.ui.adapter.PrototipoAdapter;

import java.util.List;

/**
 * Created by dev on 6/5/15.
 * Mostrar una lista de elementos de la tabla prototipo.
 *
 */
public class PrototipoListActivity extends ActionBarActivity {

    private ListView listView;
    private PrototipoAdapter prototipoAdapter;
    private List<String[]> list;


    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);

        setContentView(R.layout.activity_prototipolist);
        listView = (ListView) findViewById(R.id.list);

        prototipoAdapter = new PrototipoAdapter(this);
        listView.setAdapter(prototipoAdapter);
    }

    public void onClickAddPrototipo(View view){
        Intent intent = new Intent(PrototipoListActivity.this, PrototipoAddActivity.class);
        startActivity(intent);
    }
}
