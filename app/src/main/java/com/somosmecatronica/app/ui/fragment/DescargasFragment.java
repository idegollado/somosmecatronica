package com.somosmecatronica.app.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.somosmecatronica.app.ui.activity.MainActivity;
import com.somosmecatronica.app.R;
import com.somosmecatronica.app.data.dao.SQLiteRevistaDAO;
import com.somosmecatronica.app.main.SomosRevista;
import com.somosmecatronica.app.ui.activity.PdfActivity;
import com.somosmecatronica.app.ui.adapter.SomosGridAdapter;
import com.somosmecatronica.app.util.consts.ExtraName;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class DescargasFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    List<SomosRevista> revistasList;

    private static final String ARG_SECTION_NUMBER = "section_number";

    public DescargasFragment() {
        // Required empty public constructor
    }

    public static DescargasFragment newInstance(int param1) {
        DescargasFragment fragment = new DescargasFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, param1);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //llamar a la base de datos y mostar
        SQLiteRevistaDAO dao = new SQLiteRevistaDAO();
        revistasList = dao.select();



        View v = inflater.inflate(R.layout.fragment_descargas, container, false);
        final GridView gridview =(GridView) v.findViewById(R.id.gridview_descarga);
        SomosGridAdapter adapter = new SomosGridAdapter(getActivity(),revistasList);
        gridview.setAdapter(adapter);//setAdaptewr
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //el tipo que regresa es dependiendo de su adapter?
                SomosRevista cursor = (SomosRevista) parent.getItemAtPosition(position);
                try {

                    String fileName = String.valueOf(cursor.getEdition());//edition to String
                    Intent intent = new Intent(getActivity(), PdfActivity.class);
                    intent.putExtra(ExtraName.REVISTA_EDITION,fileName);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
      return v;
    }




    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
            ((MainActivity)activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



}
