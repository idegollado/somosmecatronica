package com.somosmecatronica.app.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.somosmecatronica.app.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by dev on 6/8/15.
 */
public class PrototipoAdapter extends BaseAdapter {

    //lista de elementos.
    String[] item1 = {"Prototipo 1", "Descripcion1 ", "10"};
    String[] item2 = {"Prototipo 2", "Descripcion2 ", "100"};
    String[] item3 = {"Prototipo 3", "Descripcion3 ", "1000"};


    ArrayList<String[]> items;
    //layout inflater
    private LayoutInflater inflater;
    private Activity activity;


    public PrototipoAdapter(Activity activity){
        this.activity = activity;

        items = new ArrayList<String[]>();
        items.add(item1);
        items.add(item2);
        items.add(item3);
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(inflater == null )
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(convertView == null)
            convertView = inflater.inflate(R.layout.layout_prototipo_item_row, null);

        //relacionamos una variable java con los elementos descritos en xml layout.
        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView desc = (TextView) convertView.findViewById(R.id.desc);
        TextView likes = (TextView) convertView.findViewById(R.id.likes);

        //Obtener la posicion un elemento de la lista para poder relacionar los datos
        String[] current = items.get(position);
        title.setText(current[0]);
        desc.setText(current[1]);
        likes.setText("likes: " + current[2]);

        //con una vista actual.

        //
            return convertView;
    }
}
