package com.somosmecatronica.app.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQueryAdapter;
import com.somosmecatronica.app.ui.activity.MainActivity;
import com.somosmecatronica.app.R;
import com.somosmecatronica.app.data.dao.SQLiteRevistaDAO;
import com.somosmecatronica.app.main.SomosRevista;
import com.somosmecatronica.app.ui.activity.DownloaderActivity;
import com.somosmecatronica.app.util.consts.ExtraName;
import com.somosmecatronica.app.ui.adapter.CustomParseAdapter;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CatalogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CatalogFragment extends Fragment implements View.OnClickListener,
SwipeRefreshLayout.OnRefreshListener{


    private static final String TAG = "CatalogFragment";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_SECTION_NUMBER = "section_number";


    // TODO: Rename and change types of parameters
    private int mParam1;


    private OnFragmentInteractionListener mListener;

    private GridView mGridView;
    private CustomParseAdapter mAdapter;
    private Button mBtnAgain;
    private ProgressBar mProgressBar;
    private SwipeRefreshLayout mCatalogRefreshLayout;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment CatalogFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CatalogFragment newInstance(int param1) {
        CatalogFragment fragment = new CatalogFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, param1);

        fragment.setArguments(args);
        return fragment;
    }

    public CatalogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_SECTION_NUMBER);

        }
        mAdapter = new CustomParseAdapter(getActivity(), "Producto");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_catalog, container, false);
        mCatalogRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.catalog_swipe_refresh_layout);
        mGridView = (GridView) rootView.findViewById(R.id.gridview);
        mBtnAgain = (Button) rootView.findViewById(R.id.reintentar_conexion);
        mProgressBar =(ProgressBar) rootView.findViewById(R.id.progress_bar_catalog);
        //visibility status
        mProgressBar.setVisibility(ProgressBar.INVISIBLE);
        mBtnAgain.setVisibility(Button.INVISIBLE);
        mGridView.setVisibility(GridView.INVISIBLE);


        if(hayConexion(getActivity())) {
            fillAdapter();
            mGridView.setVisibility(GridView.VISIBLE);
            mBtnAgain.setVisibility(Button.INVISIBLE);
        }else{
            //mostrar que no hay conexion a internet
            mBtnAgain.setVisibility(Button.VISIBLE);
            Toast.makeText(getActivity(),"Error en la Conexion", Toast.LENGTH_SHORT).show();

        }

        setOnClickAdapter();
        mBtnAgain.setOnClickListener(this);
        mCatalogRefreshLayout.setOnRefreshListener(this);
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    private void setOnClickAdapter(){
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ParseObject cursor = (ParseObject) parent.getItemAtPosition(position);
                try {
                    String fileUrlPath = cursor.getString("file");
                    String title = cursor.getString("title");
                    String fileName = cursor.getString("name");
                    String imageUrlPath = cursor.getParseFile("icon").getUrl();
                    String imageName = cursor.getParseFile("icon").getName();
                    String key = cursor.getObjectId();
                    long date = cursor.getCreatedAt().getTime();
                    int edition = cursor.getInt("edition");
                    int price = cursor.getInt("price");




                    Intent intent = new Intent(getActivity(), DownloaderActivity.class);
                    intent.putExtra("extraKey", key);
                    intent.putExtra("extraFileUrlPath", fileUrlPath);
                    intent.putExtra("extraTitle", title);
                    intent.putExtra("extraFileName", fileName);
                    intent.putExtra("extraImageUrlPath", imageUrlPath);
                    intent.putExtra("extraImageName", imageName);
                    intent.putExtra("extraDate", date);
                    intent.putExtra(ExtraName.REVISTA_EDITION, String.valueOf(edition));
                    intent.putExtra("extraPrice", price);
                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
            ((MainActivity)activity).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    @Override
    public void onClick(View v) {

        if(hayConexion(getActivity())){
            fillAdapter();
            mBtnAgain.setVisibility(Button.INVISIBLE);
            mGridView.setVisibility(GridView.VISIBLE);
        }else{
            Toast.makeText(getActivity(),"Error en la Conexion", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onRefresh() {
        updateAdapter();
    }
    //verifica si hay conexion entre el dispositivo movil y un router. Para saber si el wifi esta encendido.
    private boolean hayConexion(Context thiz){
        ConnectivityManager connManager = (ConnectivityManager)
                thiz.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = connManager.getActiveNetworkInfo();

        //verificar conexion antes de descargar
        return (netInfo != null) && netInfo.isConnected() && (netInfo.getState() == NetworkInfo.State.CONNECTED);//result 1
    }

    //verfica que el router al que se esta conectado tenga acceso a internet.
    private boolean hayInternet(){

           return  true;
    }

    private void fillAdapter(){

        mAdapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener<ParseObject>() {
            @Override
            public void onLoading() {
                //trigger any "loading UI"
                mProgressBar.setVisibility(ProgressBar.VISIBLE);
                if(!hayInternet()){
                    Toast.makeText(getActivity(),
                                   "No hay internet por favor verifique su conexion",
                                    Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onLoaded(List<ParseObject> parseObjects, Exception e) {
                if(parseObjects == null && e == null) {
                    mBtnAgain.setVisibility(Button.VISIBLE);
                    mProgressBar.setVisibility(ProgressBar.INVISIBLE);
                }else {
                    mProgressBar.setVisibility(ProgressBar.GONE);
                }
            }
        });
        mGridView.setAdapter(mAdapter);
        mGridView.deferNotifyDataSetChanged();

    }

    private void updateAdapter(){
        mAdapter = new CustomParseAdapter(getActivity(), "Producto");
        mGridView.setAdapter(mAdapter);
        mCatalogRefreshLayout.setRefreshing(false);
    }
}
