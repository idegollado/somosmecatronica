package com.somosmecatronica.app.ui.activity;

import android.content.SharedPreferences;

import android.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;

import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;

import com.joanzapata.pdfview.PDFView;
import com.joanzapata.pdfview.listener.OnPageChangeListener;
import com.joanzapata.pdfview.listener.OnLoadCompleteListener;
import com.somosmecatronica.app.util.consts.EstadoCompra;
import com.somosmecatronica.app.util.consts.ExtraName;

import com.somosmecatronica.app.R;



/**
 * Created by dev on 3/27/15.
 */
public class PdfActivity extends Activity implements OnPageChangeListener, OnLoadCompleteListener
{
    private static final String TAG = PdfActivity.class.getSimpleName();
    private static final String PAGINA_PREF = "utlima_pagina";
    private static final int PAGINA_DEFAULT = 1;

    int estadoRevista;
    int ultimaPag;
    //EXTRAS
    String editionfileName;
    File pdfFile;

    PDFView pdfView;
    ProgressBar progressBar;
    Button imagePaypal;
    LinearLayout llCompra;
    TextView tvPages;

    Bundle extras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pdf_main);

        pdfView = (PDFView) findViewById(R.id.pdfview);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        imagePaypal = (Button) findViewById(R.id.imagePaypal);
        llCompra = (LinearLayout) findViewById(R.id.ll_compra);
        tvPages = (TextView)findViewById(R.id.tv_pages);
        extras = getIntent().getExtras();


        if(extras != null){
            editionfileName = extras.getString(ExtraName.REVISTA_EDITION);
            estadoRevista = getEstadoRevista(editionfileName);
            ultimaPag = getUtlimaPagina(editionfileName);
            //if fileName exists buscar el archivo localmente
            if(editionfileName != null) {

               
                pdfFile = getFileStreamPath(editionfileName + ".pdf");
                //size == sizeofRegularFile
                if (pdfFile.exists() && pdfFile.isFile()) {

                    pdfView.fromFile(pdfFile)
                            .defaultPage(ultimaPag)
                            .onPageChange(this)
                            .onLoad(this)
                            .load();
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() { 
        super.onPause(); 
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    public void onPageChanged(int page, int pageCount) {
           //posiblemente el mensaje de comparar
        //tambien para la vista horizontal
        tvPages.setText(page + "/" + pageCount);
        setUltimaPagina(editionfileName, page);
        if(estadoRevista == EstadoCompra.INCOMPLETE) {

            if (page == pageCount) {
                //esto deberia ser un fragment o un dialog
                llCompra.setVisibility(LinearLayout.VISIBLE);
            } else {
                llCompra.setVisibility(LinearLayout.INVISIBLE);
            }
        }
    }

    @Override
    public void loadComplete(int page){
        //loading is complete
       /* Toast.makeText(this,"Complete: " + getUtlimaPagina(editionfileName), Toast.LENGTH_SHORT).show();
        pdfView.jumpTo(getUtlimaPagina(editionfileName));*/
    }

    public void onClickPayment(View pressed){
       //Abre la activity de pago
        Intent intent = new Intent(PdfActivity.this,PreviewPaymentActivity.class);
        intent.putExtras(extras);
        startActivity(intent);
    }

    private int getEstadoRevista(String edition){
        SharedPreferences pref = this.getSharedPreferences(EstadoCompra.FILENAME,Context.MODE_PRIVATE);
        return pref.getInt(edition,EstadoCompra.INCOMPLETE);
    }

    private void setUltimaPagina(String key,int page){
        SharedPreferences paginaPref = this.getSharedPreferences(PAGINA_PREF,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = paginaPref.edit();
        editor.putInt(key, page);//*
        editor.apply();
    }
    private int getUtlimaPagina(String edition){
        SharedPreferences pref = this.getSharedPreferences(PAGINA_PREF,Context.MODE_PRIVATE);
        return pref.getInt(edition,PAGINA_DEFAULT);
    }


}
