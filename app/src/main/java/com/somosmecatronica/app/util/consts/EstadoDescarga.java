package com.somosmecatronica.app.util.consts;

/**
 * Created by dev on 8/8/15.
 */
public class EstadoDescarga {

    public static final int PREPARING = 0;
    public static final int DOWNLOADING = 1;
    public static final int DOWNLOADED = 2;
    public static final int ERROR = 3;

}
