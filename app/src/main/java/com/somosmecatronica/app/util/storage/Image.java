package com.somosmecatronica.app.util.storage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by dev on 8/12/15.
 */
public class Image {

    public static Bitmap getBitmapFromFile(File file){
        Bitmap bitmap = null;
        if(file.exists()){
            try{
                FileInputStream streamIn = new FileInputStream(file);
                bitmap = BitmapFactory.decodeStream(streamIn);
                streamIn.close();
            }catch (FileNotFoundException e){

            }catch (IOException e){

            }
        }
        return bitmap;
    }
}
