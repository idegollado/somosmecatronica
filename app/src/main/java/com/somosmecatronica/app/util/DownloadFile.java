package com.somosmecatronica.app.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;

/**
 * Created by dev on 4/8/15.
 */
public class DownloadFile {

    //DESCARGAR UN ARCHIVO X, FROM/TO, MEDIANTE

    //X es el tipo de archivo que se desea descargar.
    //FROM es la direccion del archivo
    //TO es el lugar en donde se guardara el archivo.
    //OVER es el protocolo  a utililzar para realizar la operacion.

    //static String urlStr;
    URL url;
    File file;
    HttpURLConnection connection;


    public DownloadFile(){

    }


    public void from(String urlStr) {
        try {
            URL url = new URL(urlStr);

        }catch(MalformedURLException me){
            me.printStackTrace();
        }

    }


    public void from(URL pUrl){
        url = pUrl;

    }

    public void to(File pFile){
        file = pFile;

    }

    private void writeStreamFromTo(InputStream in, OutputStream out){
        int NULL = -1;
        byte [] buffer = new byte[1024]; //number of bytes to read and write per cycle
        try {
            int count = in.read(buffer);
            while(count != NULL){
                out.write(buffer,0,count);
                in.read(buffer);
            }
        }catch(IOException e){

        }
    }
    public  boolean init(){

        FileOutputStream streamOut = null;
        InputStream streamIn = null;

        try {
            streamOut = new FileOutputStream(file);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            if(connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                Log.i("downloadFile","HTTP_OK");

                streamIn = connection.getInputStream();

                writeStreamFromTo(streamIn,streamOut);
                streamIn.close();
                streamOut.close();
                connection.disconnect();

                return true;

            }else{
                Log.i("downloadFile","Error al conectar");

            }
        }catch(IOException ioe){

        }
        return false;
    }


}
