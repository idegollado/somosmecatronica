package com.somosmecatronica.app.util.mobile.settings;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

import com.somosmecatronica.app.GlobalApp;

/**
 * Created by dev on 7/8/15.
 */
public class Cuenta {

    private static AccountManager accountManager = AccountManager.get(GlobalApp.getAppContext());
    private static Account account=null;


    public static String getGmail(){
        return getAccount("com.google");
    }

    public static String getAccount( String type){

        Account[] accounts = accountManager.getAccountsByType(type);
        account = null;

        if(accounts.length > 0){
            account = accounts[0];
            return account.name;
        }

        return "";
    }
}
