package com.somosmecatronica.app.util.consts;

/**
 * Created by dev on 8/2/15.
 */
public class EstadoCompra {
    public static final String FILENAME="estado_compra";

    public  static final  int INCOMPLETE = 0;
    public static final int COMPLETE = 1;
}
