package com.somosmecatronica.app.util.intent;

public class DownloadIntentConstant {

	// Set to true to turn on debug logging
    public static final boolean LOGD = false;
	
	public static final String EXTENDED_STATUS_LOG = "com.somosmecatronica.app.LOG";	

	public static final String BROADCAST_ACTION = "com.somosmecatronica.app.BROADCAST";
	
	public static final String EXTENDED_DATA_STATUS = "com.somosmecatronica.app.STATUS";

	// Status values to broadcast to the Activity

    // The download is starting
    public static final int STATE_ACTION_STARTED = 0;

    // The background thread is connecting to the RSS feed
    public static final int STATE_ACTION_CONNECTING = 1;

    // The background thread is parsing the RSS feed
    public static final int STATE_ACTION_PARSING = 2;

    // The background thread is writing data to the content provider
    public static final int STATE_ACTION_WRITING = 3;

    // The background thread is done
    public static final int STATE_ACTION_COMPLETE = 4;

    // The background thread is doing logging
    public static final int STATE_LOG = -1;
}