package com.somosmecatronica.app.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by dev on 4/12/15.
 */
public class  IOMethods {

    public static void writeStreamFromTo(InputStream in, OutputStream out){
        int NULL = -1;
        try {
            byte [] buffer = new byte[1024]; //number of bytes to read and write per cycle
      
            int count = in.read(buffer);
            while(count != NULL){
                out.write(buffer,0,count);
                count = in.read(buffer);
            }
            out.flush();
            out.close();
            in.close();
        }catch(IOException e){

        }
    }
    public static void writeStreamFromToFast(InputStream in, OutputStream out){

    }
}
