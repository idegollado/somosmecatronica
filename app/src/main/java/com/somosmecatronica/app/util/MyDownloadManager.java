package com.somosmecatronica.app.util;

import java.io.BufferedInputStream;
import java.io.IOException;
import android.net.NetworkInfo;
import android.util.Log;
import java.net.MalformedURLException;
import java.net.HttpURLConnection;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import android.net.ConnectivityManager;
import android.content.Context;

import com.somosmecatronica.app.data.dao.SQLiteDescargaDAO;
import com.somosmecatronica.app.main.SomosDescarga;
import com.somosmecatronica.app.util.IOMethods;
import com.somosmecatronica.app.util.consts.EstadoDescarga;
import com.somosmecatronica.app.util.intent.BroadcastNotifier;
import com.somosmecatronica.app.util.intent.DownloadIntentConstant;

public class MyDownloadManager {

	public static final String TAG = MyDownloadManager.class.getSimpleName();

	private Context thiz;
	private String urlStr;
	private String edition;
	private BroadcastNotifier notifier;

	public MyDownloadManager(Context context, String urlString, String pEdition) {
		thiz = context;
		urlStr = urlString;
		edition = pEdition;
		notifier = new BroadcastNotifier(thiz);

	}

	public void execute() {

		//set

		ConnectivityManager connectivity;
		NetworkInfo netInfo;
		URL link;
		HttpURLConnection httpConnection;
		InputStream downloadInFile=null;
		FileOutputStream localOutFile;
		
		connectivity = (ConnectivityManager) thiz.getSystemService(Context.CONNECTIVITY_SERVICE);
		netInfo = connectivity.getActiveNetworkInfo();
		
		if(netInfo != null && netInfo.isConnected()) {	
			
			notifier.broadcastIntentWithState(DownloadIntentConstant.STATE_ACTION_STARTED);
			updateState(EstadoDescarga.PREPARING);
			try{

			 	link = new URL(urlStr);
				httpConnection = (HttpURLConnection) link.openConnection();
				httpConnection.setReadTimeout(10000);
				httpConnection.setConnectTimeout(15000);
				httpConnection.setDoInput(true);
				httpConnection.connect();
			 	localOutFile = thiz.openFileOutput(edition + ".pdf", Context.MODE_PRIVATE);
			 	notifier.broadcastIntentWithState(DownloadIntentConstant.STATE_ACTION_CONNECTING);
			 	
			 	if(httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK )	{

					updateState(EstadoDescarga.DOWNLOADING);
					notifier.broadcastIntentWithState(DownloadIntentConstant.STATE_ACTION_PARSING);
					downloadInFile =  new BufferedInputStream(link.openStream(),8192);
					notifier.broadcastIntentWithState(DownloadIntentConstant.STATE_ACTION_WRITING);
	                IOMethods.writeStreamFromTo(downloadInFile, localOutFile);
	              	notifier.broadcastIntentWithState(DownloadIntentConstant.STATE_ACTION_COMPLETE, edition);
	                updateState(EstadoDescarga.DOWNLOADED);
					httpConnection.disconnect();

				}else{
					//error en la conexion fuera de el dispositivo.(Servers)
					updateState(EstadoDescarga.ERROR);
				}
			}catch (MalformedURLException e){//new URL(name)

	        }catch(IOException e){//url.openConnection

        	}

        }else{
			Log.e(TAG, "Verifique que su dispositivo esta conectado a internet");
			updateState(EstadoDescarga.ERROR);
		}
	}

	private void updateState(int state) {
		SQLiteDescargaDAO sqliteDescargaQuery = new SQLiteDescargaDAO();
		List<SomosDescarga> descargasByRevistas =
				sqliteDescargaQuery.selectByRevista(Integer.parseInt(edition));
			int countDownloads = descargasByRevistas.size();

			if (countDownloads == 0) {

				//no existe?, registrala.
				SomosDescarga descarga = new SomosDescarga();
				descarga.setKey(urlStr);
				descarga.setEstado(state);
				descarga.setRevista(Integer.parseInt(edition));
				sqliteDescargaQuery.insert(descarga);
			} else if (countDownloads == 1) {
				//existe una, actualiza su estado.
				SomosDescarga descarga = descargasByRevistas.get(0);

				if(descarga.getEstado() != EstadoDescarga.DOWNLOADED) {
					sqliteDescargaQuery.updateEstado(descarga.getId(), state);

				}
			} else {
				Log.e(TAG, "Error (No es permitido haya mas de un registros con el mismo valor de revista)");
			}
	}
}