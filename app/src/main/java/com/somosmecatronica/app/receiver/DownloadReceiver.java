package com.somosmecatronica.app.receiver;

import android.app.PendingIntent;
import android.content.SharedPreferences;
import android.util.Log;
import android.content.Intent;
import android.content.Context;
import android.content.BroadcastReceiver;

import com.somosmecatronica.app.ui.activity.DownloaderActivity;
import com.somosmecatronica.app.ui.activity.PdfActivity;
import com.somosmecatronica.app.ui.notification.NotificationBuilder;
import com.somosmecatronica.app.util.consts.EstadoCompra;
import com.somosmecatronica.app.util.consts.ExtraName;
import com.somosmecatronica.app.util.intent.DownloadIntentConstant;

public class DownloadReceiver extends BroadcastReceiver{

	public static final String TAG = DownloadReceiver.class.getSimpleName();

	private DownloadReceiverDelegate delegate;
	private NotificationBuilder notification;

	public DownloadReceiver(DownloadReceiverDelegate deleg) {
		delegate = deleg;
	}

	@Override
	public void onReceive(Context context, Intent intent) {

		notification = new NotificationBuilder(context);
		notification.setTitle("Revista SomosMecatronica");
		notification.setMessage("Tu revista se esta descargando");
		notification.setAutoCancel(true);
		notification.setActivityResult(DownloaderActivity.class);
		notification.setPendingIntentFlag(PendingIntent.FLAG_NO_CREATE);


		if(delegate != null) {

	        switch (intent.getIntExtra(DownloadIntentConstant.EXTENDED_DATA_STATUS, 
	        						   DownloadIntentConstant.STATE_ACTION_COMPLETE))	{
	
		        	 // Logs "started" state
	                case DownloadIntentConstant.STATE_ACTION_STARTED:
	                    if (DownloadIntentConstant.LOGD) 
	                        Log.d(TAG, "State: STARTED");
	                    delegate.started();
						notification.build();
	                    break;
	                // Logs "connecting to network" state
	                case DownloadIntentConstant.STATE_ACTION_CONNECTING:
	                    if (DownloadIntentConstant.LOGD)
	                        Log.d(TAG, "State: CONNECTING");
	                	delegate.connecting();
	                    break;
	                 // Logs "parsing the RSS feed" state
	                 case DownloadIntentConstant.STATE_ACTION_PARSING:
	                    if (DownloadIntentConstant.LOGD)
	                        Log.d(TAG, "State: PARSING");
	                    delegate.parsing();
	                    break;
	                // Logs "Writing the parsed data to the content provider" state
	                case DownloadIntentConstant.STATE_ACTION_WRITING:
	                    if (DownloadIntentConstant.LOGD) 
	                        Log.d(TAG, "State: WRITING");
	                    delegate.writing();
	                    break;
	                // Starts displaying data when the RSS download is complete
	                case DownloadIntentConstant.STATE_ACTION_COMPLETE:
	                    // Logs the status
	                    if (DownloadIntentConstant.LOGD)
	                        Log.d(TAG, "State: COMPLETE");

	                    String edition = intent.getStringExtra(ExtraName.REVISTA_EDITION);
						Log.d(TAG, edition);
	                    delegate.complete(null, edition);

						SharedPreferences preferences = context.getSharedPreferences(EstadoCompra.FILENAME,Context.MODE_PRIVATE);
						SharedPreferences.Editor editor = preferences.edit();
						editor.putInt(edition,EstadoCompra.COMPLETE);
						editor.commit();

						notification.setMessage("Revista descargada exitosamente");
						notification.setExtra(ExtraName.REVISTA_EDITION, edition);
						notification.setPendingIntentFlag(PendingIntent.FLAG_UPDATE_CURRENT);
						notification.build();
						break;
						
	                default:
	                    break;
	                }
        	}
	}
}