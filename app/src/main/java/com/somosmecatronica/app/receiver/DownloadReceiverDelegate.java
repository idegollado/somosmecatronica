package com.somosmecatronica.app.receiver;

public interface DownloadReceiverDelegate{

	public void started();
	public void connecting();
	public void parsing();
	public void writing();
	public void complete(String path, String fileName);
}
