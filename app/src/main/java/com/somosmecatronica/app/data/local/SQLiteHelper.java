package com.somosmecatronica.app.data.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by dev on 3/22/15.
 */
public class SQLiteHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION=1;
    private static final String DB_NAME="revistasm.db";

    private static SQLiteHelper ourInstance;

    public static synchronized SQLiteHelper getInstance(Context context) {

        if (ourInstance == null)
            ourInstance = new SQLiteHelper(context);
        return ourInstance;

    }

    private SQLiteHelper(Context context) {
        super(context,DB_NAME,null,DB_VERSION);
    }


    //methods


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SchemaTablaRevista.SQL_CREATE_ENTRIES);
        db.execSQL(SchemaDescarga.SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SchemaTablaRevista.SQL_DELETE_ENTRIES);
        db.execSQL(SchemaDescarga.SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }

}
