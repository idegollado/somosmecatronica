package com.somosmecatronica.app.data.third;

import com.somosmecatronica.app.data.Sync;
import com.somosmecatronica.app.main.SomosObject;

import java.util.Date;
import java.util.List;

/**
 * Created by dev on 3/2/15.
 * * Esta clase syncroniza los datos de la base local con los datos del servidor
 */
public interface CloudData {

    public void setSynchronizator(Sync sync);
    public Sync getSynchronizator();
    public void insert();//nada para ingresar?
    public void delete();//nada para borrar
    public void synchronize();
    public void update();
}


