package com.somosmecatronica.app.data.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;


import com.somosmecatronica.app.GlobalApp;
import com.somosmecatronica.app.data.local.SQLiteHelper;
import com.somosmecatronica.app.main.SomosDescarga;
import com.somosmecatronica.app.main.SomosRevista;

import static com.somosmecatronica.app.data.local.SchemaDescarga.Column.*;
import static com.somosmecatronica.app.data.local.SchemaDescarga.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dev on 8/7/15.
 */
public class SQLiteDescargaDAO implements DescargaDAO {

    SQLiteHelper helper = SQLiteHelper.getInstance(GlobalApp.getAppContext());
    SQLiteDatabase db = helper.getWritableDatabase();

    @Override
    public int insert(List<SomosDescarga> descargas) {
        int numInserted=0;
        boolean inserted=true;

        for(SomosDescarga descarga :descargas) {
            while(inserted){
                inserted = insert(descarga);
                numInserted++;
            }
        }

        return numInserted;
    }

    @Override
    public boolean insert(SomosDescarga descarga) {
        boolean inserted =false;

        ContentValues values = new ContentValues();
        values.put(REVISTA,descarga.getRevista());
        values.put(ESTADO, descarga.getEstado());
        values.put(KEY, descarga.getKey());

        inserted = db.insertOrThrow(NAME,null,values) == -1 ? false:true;

        return inserted;
    }

    @Override
    public List<SomosDescarga> selectAll() {

        final Cursor cursor = db.query(NAME,null,null,null,null,null,null);
        List<SomosDescarga> descargas = new ArrayList<SomosDescarga>();
        try{
            while (cursor.moveToNext()){
                SomosDescarga descarga = new SomosDescarga();
                descarga.setId(cursor.getInt(cursor.getColumnIndex(_ID)));
                descarga.setRevista(cursor.getInt(cursor.getColumnIndex(REVISTA)));
                descarga.setEstado(cursor.getInt(cursor.getColumnIndexOrThrow(ESTADO)));
                descarga.setKey(cursor.getString(cursor.getColumnIndex(KEY)));

                descargas.add(descarga);
            }
        }catch(CursorIndexOutOfBoundsException e){

        }finally {
            cursor.close();
        }

        return descargas;
    }

    @Override
    public List<SomosDescarga> selectById(int id) {
        return null;
    }

    @Override
    public List<SomosDescarga> selectByRevista(int id) {

        String whereClause = REVISTA + "= ?";
        String[] whereValues = new String[]{String.valueOf(id)};
        final Cursor cursor = db.query(NAME,null,whereClause,whereValues,null,null,null);
        List<SomosDescarga> descargas = new ArrayList<SomosDescarga>();

        try{
            while (cursor.moveToNext()){
                SomosDescarga descarga = new SomosDescarga();
                descarga.setId(cursor.getInt(cursor.getColumnIndex(_ID)));
                descarga.setRevista(cursor.getInt(cursor.getColumnIndex(REVISTA)));
                descarga.setEstado(cursor.getInt(cursor.getColumnIndexOrThrow(ESTADO)));
                descarga.setKey(cursor.getString(cursor.getColumnIndex(KEY)));

                descargas.add(descarga);
            }
        }catch(CursorIndexOutOfBoundsException e){
            e.printStackTrace();
        }finally {
            cursor.close();
        }

        return descargas;
    }

    @Override
    public List<SomosDescarga> selectByEstado(int id) {
        return null;
    }

    @Override
    public List<SomosDescarga> selectByKey(int id) {
        return null;
    }

    @Override
    public boolean update(int idObject, SomosDescarga descarga) {
        return false;
    }

    @Override
    public boolean updateEstado(int idObject, int estado) {


        int updated;
        ContentValues values = new ContentValues();
        values.put(ESTADO,estado);

        String selection = _ID + " = ? ";
        String[] selectionArgs = new String[]{String.valueOf(idObject)};

        updated = db.update(NAME, values, selection, selectionArgs);

        return updated <=  1 ? false : true;
    }

    @Override
    public boolean exists(int id) {
        return false;
    }
}
