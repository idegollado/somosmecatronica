package com.somosmecatronica.app.data.third;

import android.content.Context;
import android.support.annotation.NonNull;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.somosmecatronica.app.data.Sync;
import com.somosmecatronica.app.data.UserSettings;
import com.somosmecatronica.app.main.SomosRevista;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import com.somosmecatronica.app.R;
/**
 * Created by dev on 3/10/15.
 */
public class AbstractCloudData implements CloudData {
    private Sync sync;
    protected ParseQuery<ParseObject> query;
    protected Context context;

    public AbstractCloudData(Context context, String table){
        this.context = context;

        Parse.initialize(context,
                         context.getString(R.string.parse_app_id),
                         context.getString(R.string.parse_client_key));

        //name is dynamic
        query = ParseQuery.getQuery(table);
        //query = ParseQuery.getQuery(table);

    }

    @Override
    public void setSynchronizator(Sync sync) {
        this.sync = sync;
    }

    @Override
    public void insert() {

    }

    @Override
    public void delete() {

    }

    @Override
    public void synchronize() {

    }

    @Override
    public void update() {

    }
    @Override
    public Sync getSynchronizator(){
        return this.sync;
    }
}
