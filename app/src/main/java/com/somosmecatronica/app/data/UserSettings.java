package com.somosmecatronica.app.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.somosmecatronica.app.R;

import java.util.Date;

/**
 * Created by dev on 3/10/15.
 */
public class UserSettings {

    static SharedPreferences prefer;

    private static final String SETTINGS = "user_settings";

    public static Date getLastUpdateDate(Context context){
        prefer = context.getSharedPreferences(SETTINGS,Context.MODE_PRIVATE);
        long dateInMillis = prefer.getLong(context.getString(R.string.last_date),0L);
        return new Date(dateInMillis);

    }

    public static void setLastUpdateDate(Context context, Date date){

        long dateInMillis = date.getTime();
        setLastUpdateDate(context, dateInMillis);
    }

    public static void setLastUpdateDate(Context context, long millis){
        prefer = context.getSharedPreferences(SETTINGS,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefer.edit();

        editor.putLong(context.getString(R.string.last_date),millis);
        editor.commit();

    }
}
