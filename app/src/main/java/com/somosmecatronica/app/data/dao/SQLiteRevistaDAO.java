package com.somosmecatronica.app.data.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;

import com.somosmecatronica.app.GlobalApp;
import com.somosmecatronica.app.data.local.SQLiteHelper;
import com.somosmecatronica.app.data.local.SchemaTablaRevista;
import com.somosmecatronica.app.main.SomosDescarga;
import com.somosmecatronica.app.main.SomosRevista;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.provider.BaseColumns._ID;
import static com.somosmecatronica.app.data.local.SchemaTablaRevista.Column.*;
import static com.somosmecatronica.app.data.local.SchemaTablaRevista.*;


/**
 * Created by dev on 3/22/15.
 */
public class SQLiteRevistaDAO implements  RevistaDAO {

    public SQLiteRevistaDAO() {

    }
    @Override
    public boolean insert(List<SomosRevista> revistas) {
        //get sqlite database instance though of sqlitehelper
        boolean tag_insert=false;
        SQLiteDatabase dbSqlite = SQLiteHelper.getInstance(
                                    GlobalApp.getAppContext() ).getWritableDatabase();

        //convert revistas into contentValues list
        for(SomosRevista revista : revistas)
             tag_insert = insert(revista);

        return tag_insert;
    }

    @Override
    public boolean insert(SomosRevista revista) {
        long tag_insert=-1;
        SQLiteDatabase dbSqlite = SQLiteHelper.getInstance(
                GlobalApp.getAppContext()).getWritableDatabase();

        //convert revistas into contentValues list
        ContentValues values = new ContentValues();
        values.put(UNIQUE_ID, revista.getKey());
        values.put(ICON_NAME, revista.getIconName());
        values.put(DATE, revista.getDate().getTime());
        values.put(EDITION, revista.getEdition());
        values.put(STATE, revista.getState());
        values.put(TITLE, revista.getTitle());
        values.put(FILE_NAME,revista.getFileName());
        values.put(FILE_URL, revista.getFileUrl());
        values.put(PRICE, revista.getPrice());


        tag_insert = dbSqlite.insert(NAME,null,values);

        return tag_insert == -1 ? false : true;
    }

    @Override
    public List<SomosRevista> select() {
        //obtener datos da la base de datos local.
            SQLiteDatabase dbSqlite = SQLiteHelper.getInstance(GlobalApp.getAppContext()).getReadableDatabase();
        //preparar query

            //query(columns = null for all)
            final Cursor c = dbSqlite.query(NAME, null,null,null,null,null,null);
            //from cursot to domain
             List<SomosRevista> revistas = new ArrayList<>();
            try {

                while(c.moveToNext()){
                    SomosRevista revista = new SomosRevista();

                    revista.setKey(c.getString(c.getColumnIndex(UNIQUE_ID)));
                    revista.setDate(new Date(c.getLong(c.getColumnIndex(DATE))));
                    revista.setFileName(c.getString(c.getColumnIndex(FILE_NAME)));
                    revista.setFileUrl(c.getString(c.getColumnIndex(FILE_URL)));
                    revista.setIconName(c.getString(c.getColumnIndex(ICON_NAME)));
                    revista.setTitle(c.getString(c.getColumnIndex(TITLE)));
                    revista.setEdition(c.getInt(c.getColumnIndex(EDITION)));
                    revista.setState(c.getInt(c.getColumnIndex(STATE)));
                    revista.setPrice(c.getInt(c.getColumnIndex(PRICE)));
                    revistas.add(revista);
                }
            }catch(Exception e){

            }finally{
                c.close();
            }

            // devlover los datos en forma de dominio

        return revistas;
    }

    @Override
    public List<SomosRevista> selectByDate(Date date) {
        return null;
    }

    @Override
    public List<SomosRevista> selectByRevista(int edition) {
        //obtener datos da la base de datos local.
        SQLiteDatabase dbSqlite = SQLiteHelper.getInstance(GlobalApp.getAppContext()).getReadableDatabase();

        String whereClause = EDITION + "= ?";
        String[] whereValues = new String[]{String.valueOf(edition)};
        final Cursor cursor = dbSqlite.query(NAME,null,whereClause,whereValues,null,null,null);
        List<SomosRevista> revistas = new ArrayList<SomosRevista>();

        try{
            while (cursor.moveToNext()){
                SomosRevista revista = new SomosRevista();
                revista.setId(cursor.getInt(cursor.getColumnIndex(_ID)));
                revista.setPrice(cursor.getInt(cursor.getColumnIndex(PRICE)));
                revista.setFileName(cursor.getString(cursor.getColumnIndex(FILE_NAME)));
                revista.setKey(cursor.getString(cursor.getColumnIndex(UNIQUE_ID)));
                revista.setIconName(cursor.getString(cursor.getColumnIndex(ICON_NAME)));
                revista.setFileUrl(cursor.getString(cursor.getColumnIndex(FILE_URL)));
                revista.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
                revista.setState(cursor.getInt(cursor.getColumnIndex(STATE)));
                revista.setPrice(cursor.getInt(cursor.getColumnIndex(PRICE)));
                revistas.add(revista);
            }
        }catch(CursorIndexOutOfBoundsException e){
            e.printStackTrace();
        }finally {
            cursor.close();
        }

        return revistas;
    }


    @Override
    public boolean update() {
        return false;
    }

    @Override
    public boolean exists(int id) {
        return false;
    }
}
