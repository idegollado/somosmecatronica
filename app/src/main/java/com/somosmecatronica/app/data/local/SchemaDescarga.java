package com.somosmecatronica.app.data.local;

import android.provider.BaseColumns;

/**
 * Created by dev on 8/6/15.
 */
public class SchemaDescarga {

    public static final String NAME = "Descarga";
    private static final String TEXT_TYPE = " TEXT";
    private static final String INT_TYPE = " INTEGER";
    public static final String COMMA = " ,";

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE IF NOT EXISTS " + NAME + " (" +
                    Column._ID + " INTEGER PRIMARY KEY AUTOINCREMENT " +  COMMA +
                    Column.REVISTA + INT_TYPE +  " NOT NULL" +COMMA +
                    Column.ESTADO + INT_TYPE + " NOT NULL"+COMMA +
                    Column.KEY + TEXT_TYPE +
                    " )" ;

    public static final String SQL_DELETE_ENTRIES =
            "DELETE TABLE IF EXISTS " + NAME;

    public class Column implements BaseColumns{
        public static final String REVISTA = "revista";
        public static final String ESTADO = "estado";
        public static final String KEY = "key";

    }
}
