package com.somosmecatronica.app.data.dao;

import com.somosmecatronica.app.main.SomosDescarga;

import java.util.List;

/**
 * Created by dev on 8/7/15.
 */
public interface DescargaDAO {
    //devuelve el numero de elementos insertados exitosamente.
    public int insert(List<SomosDescarga> descargas);
    //devuelve true si el elemento se inserto correctamente;
    public boolean insert(SomosDescarga descarga);
    public List<SomosDescarga> selectAll();
    public List<SomosDescarga> selectById(int id);
    public List<SomosDescarga> selectByRevista(int id);
    public List<SomosDescarga> selectByEstado(int id);
    public List<SomosDescarga> selectByKey(int id);
    public boolean update(int idObject, SomosDescarga descarga);
    public boolean updateEstado(int idObject, int estado);
    public boolean exists(int id);
}
