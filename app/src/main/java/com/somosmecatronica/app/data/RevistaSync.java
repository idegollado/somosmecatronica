package com.somosmecatronica.app.data;

import android.widget.Toast;

import com.somosmecatronica.app.GlobalApp;
import com.somosmecatronica.app.data.dao.SQLiteRevistaDAO;
import com.somosmecatronica.app.main.SomosObject;
import com.somosmecatronica.app.main.SomosRevista;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dev on 3/4/15.
 */
public class RevistaSync implements Sync {


    @Override
    public void onGetCallback(List<? extends SomosObject> objs) {


    }

    @Override
    public void onSyncCallback(List<? extends SomosObject> objs) {
        //update local database
       List<SomosRevista> revistas = (List<SomosRevista>)(List<?>)objs;
       SQLiteRevistaDAO liteRevistaDAO = new SQLiteRevistaDAO();
       liteRevistaDAO.insert(revistas);


    }

    @Override
    public void onInsert(List<? extends SomosObject> objs) {

    }

    @Override
    public void onDelete(List<? extends SomosObject> objs) {

    }

    @Override
    public void onUpdate(SomosObject objs) {

    }
}
