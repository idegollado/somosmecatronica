package com.somosmecatronica.app.data.third;

import android.content.Context;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.somosmecatronica.app.GlobalApp;
import com.somosmecatronica.app.data.Sync;
import com.somosmecatronica.app.data.UserSettings;
import com.somosmecatronica.app.main.SomosRevista;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dev on 3/10/15.
 */
public class RevistaCloudData extends AbstractCloudData {

    private static final String TABLE = "Producto";

    //flags
    public boolean  callbackIsDone;
    public boolean queryIsEmpty;


    public RevistaCloudData(Context context){

        super(context,TABLE);//Revista
        callbackIsDone = false;
        queryIsEmpty = true;
    }

    @Override
    public void setSynchronizator(Sync sync) {
        super.setSynchronizator(sync);

    }

    @Override
    public void insert() {
        super.insert();
    }

    @Override
    public void delete() {
        super.delete();
    }

    @Override
    public void synchronize() {

        super.synchronize();

        //query.whereGreaterThanOrEqualTo("updatedAt", UserSettings.getLastUpdateDate(context));
        try {
            query.whereGreaterThanOrEqualTo("updatedAt", UserSettings.getLastUpdateDate(context));
            List<ParseObject> obs = query.find();

            if(!obs.isEmpty()) {
                UserSettings.setLastUpdateDate(context, System.currentTimeMillis());
                List<SomosRevista> revistas = new ArrayList<SomosRevista>();
                for (ParseObject object : obs) {
                    SomosRevista revista = new SomosRevista();
                    revista.setKey(object.getString("objectId"));
                    revista.setTitle(object.getString("name"));
                    revista.setIconName(object.getString("icon"));
                    revista.setDate(object.getUpdatedAt());
                    revista.setEdition(object.getInt("edition"));
                    revista.setFileName(object.getString("file"));
                    revista.setPrice(object.getInt("price"));
                    revista.setState(0);

                    revistas.add(revista);
                    Toast.makeText(context, revista.getFileName(), Toast.LENGTH_SHORT).show();
                }
                getSynchronizator().onSyncCallback(revistas);
            }
        }catch(ParseException e){

        }



    }

    @Override
    public void update() {
        super.update();
    }
}
