package com.somosmecatronica.app.data.local;

import android.provider.BaseColumns;

/**
 * Created by dev on 3/22/15.
 */
public class SchemaTablaRevista {

    public SchemaTablaRevista(){}

    public static final String NAME="Revista";
    private static final String TEXT_TYPE = " TEXT";
    private static final String INT_TYPE = " INTEGER";
    private static final String LONG_TYPE = " LONG";
    private static final String COMMA_SEP = " ,";
    public static final String SQL_CREATE_ENTRIES =
        "CREATE TABLE IF NOT EXISTS " + SchemaTablaRevista.NAME + " (" +
        Column._ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
        Column.UNIQUE_ID + TEXT_TYPE + COMMA_SEP +
        Column.DATE + LONG_TYPE + COMMA_SEP +
        Column.FILE_NAME + TEXT_TYPE + COMMA_SEP +
        Column.FILE_URL + TEXT_TYPE + COMMA_SEP +
        Column.ICON_NAME + TEXT_TYPE + COMMA_SEP +
        Column.TITLE + TEXT_TYPE + COMMA_SEP +
        Column.EDITION + INT_TYPE + COMMA_SEP +
        Column.PRICE + INT_TYPE + COMMA_SEP +
        Column.STATE + INT_TYPE +
        " )";

    public  static final String SQL_DELETE_ENTRIES =
    "DROP TABLE IF EXISTS " + SchemaTablaRevista.NAME;

    public static abstract class Column implements BaseColumns{

        public static final String UNIQUE_ID ="unique_id";
        public static final String ICON_NAME="icon";
        public static final String DATE="cratedAt";
        public static final String FILE_NAME="name";
        public static final String FILE_URL ="file";
        public static final String TITLE="title";
        public static final String EDITION="edition";
        public static final String PRICE = "price";
        public static final String STATE = "state";

    }
}
