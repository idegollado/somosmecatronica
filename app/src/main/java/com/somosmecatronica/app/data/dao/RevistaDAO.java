package com.somosmecatronica.app.data.dao;

import com.somosmecatronica.app.main.SomosRevista;

import java.util.Date;
import java.util.List;

/**
 * Created by dev on 3/22/15.
 */
public interface RevistaDAO {

    public boolean insert(List<SomosRevista> revistas);
    public boolean insert(SomosRevista revista);
    public List<SomosRevista> select();
    public List<SomosRevista> selectByDate(Date date);
    public List<SomosRevista> selectByRevista(int revista);
    public boolean update();
    public boolean exists(int id);


}
