package com.somosmecatronica.app.data;

import com.somosmecatronica.app.main.SomosObject;

import java.util.List;

/**
 * Created by dev on 3/4/15.
 */
public interface Sync {

    public void onGetCallback(List<? extends SomosObject> objs);
    public void onSyncCallback(List<? extends SomosObject> objs);
    public void onInsert(List<? extends SomosObject> objs);
    public void onDelete(List<? extends SomosObject> objs);
    public void onUpdate(SomosObject objs);
}
